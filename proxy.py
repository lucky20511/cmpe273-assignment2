#!/usr/bin/python
# This is a simple port-forward / proxy, written using only the default python
# library. If you want to make a suggestion or fix something you can contact-me
# at voorloop_at_gmail.com
# Distributed over IDC(I Don't Care) license
import socket
import select
import time
import sys
from cb import CircuitBreaker
from flask import json

@CircuitBreaker(max_failure_to_open=3, reset_timeout=3)
def forward_2(ip, port):
    forward = Forward().start(ip, port)
    if forward:
        return forward 
    else:
        raise Exception('[CMPE-273 ERROR] Connection is not opend')


def delete_node(list):
    del list[-1]


#node_list = []
node_list = []


# Changing the buffer_size and delay, you can improve the speed and bandwidth.
# But when buffer get to high or delay go too down, you can broke things
buffer_size = 4096
delay = 0.0001

#node_list = ('192.168.99.100', 5000)
#node_list = [('localhost', 5000)]

class Forward:
    def __init__(self):
        self.forward = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def start(self, host, port):
        try:
            self.forward.connect((host, port))
            return self.forward
        except Exception, e:
            print e
            return False

class TheServer:
    input_list = []
    channel = {}

    def __init__(self, host, port):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((host, port))
        self.server.listen(200)

        self.server_reg = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_reg.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_reg.bind((host, 9091))
        self.server_reg.listen(200)
        self.count = 0

    def main_loop(self):
        self.input_list.append(self.server)
        self.input_list.append(self.server_reg)
        while 1:
            time.sleep(delay)
            ss = select.select
            inputready, outputready, exceptready = ss(self.input_list, [], [])
            for self.s in inputready:
                # proxy port
                if self.s == self.server:
                    try:
                        self.on_accept()
                    except Exception, ex:
                        print ex.message  
                    break
                # registration port
                elif self.s == self.server_reg:
                    self.on_accept_reg()
                    break
                

                self.data = self.s.recv(buffer_size)
                if len(self.data) == 0:
                    self.on_close()
                    break
                else:
                    self.on_recv()

    def on_accept(self):
        if(len(node_list) == 0):
            self.server.accept()
            raise Exception('[CMPE-273 ERROR] Node list is Empty')

        self.count = self.count + 1
        try:
            forward = forward_2(node_list[self.count%len(node_list)][0], node_list[self.count%len(node_list)][1])
        except Exception as ex:
            self.server.accept()
            self.count = self.count - 1
            print ex.message
            if(str(ex.message).find("CircuitBreaker") == 0):
                print "[CMPE-273] Delete Node"
                delete_node(node_list)
            return

        clientsock, clientaddr = self.server.accept()
        if forward:
            print clientaddr, "has connected"
            self.input_list.append(clientsock)
            self.input_list.append(forward)
            self.channel[clientsock] = forward
            self.channel[forward] = clientsock


    def on_accept_reg(self):
        clientsock, clientaddr = self.server_reg.accept()
        
        data = clientsock.recv(1024)
        string = bytes.decode(data)

        tmp = string.split("?")[-1]
        tmp = tmp.split("&")
        ip = tmp[0]
        port = tmp[1].split(" ")[0]
        #print "!!! " + string.split("?")[-1]
        #print ip[3:len(ip)]
        #print port[5:len(port)]
        print "[CMPE-273] Add Node: ip => " + ip[3:len(ip)] + "  port => " + port[5:len(port)]
        node_list.append( ( ip[3:len(ip)], int(port[5:len(port)]) ) )

    def on_close(self):
        print self.s.getpeername(), "has disconnected"
        #remove objects from input_list
        self.input_list.remove(self.s)
        self.input_list.remove(self.channel[self.s])
        out = self.channel[self.s]
        # close the connection with client
        self.channel[out].close()  # equivalent to do self.s.close()
        # close the connection with remote server
        self.channel[self.s].close()
        # delete both objects from channel dict
        del self.channel[out]
        del self.channel[self.s]

    def on_recv(self):
        data = self.data
        # here we can parse and/or modify the data before send forward
        print data
        self.channel[self.s].send(data)

if __name__ == '__main__':
        server = TheServer('127.0.0.1', 9090)
        try:
            server.main_loop()
        except KeyboardInterrupt:
            print "Ctrl C - Stopping server"
            sys.exit(1)